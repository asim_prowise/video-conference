package com.example.videoconferenceapp

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.videoconferenceapp.broadcastReceiver.SSOBroadcastReceiver
import com.example.videoconferenceapp.constants.Constants
import com.example.videoconferenceapp.constants.Constants.REQUEST_ACCESS_TOKEN_SCREENSHARING
import com.example.videoconferenceapp.serviceHelper.SSOServiceHelper

class CommuteActivity : AppCompatActivity(), View.OnClickListener {

    private var buttonBindService: Button? = null
    private var buttonUnBindService: Button? = null
    private var buttonGetInfo: Button? = null

    private var ssoBroadcastReceiver: SSOBroadcastReceiver? = null

    private val TAG = "CommuteActivity"

    companion object {
        val instance = CommuteActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_commute)

        val filter = IntentFilter()
        filter.addAction(Constants.SCREEN_SHARING_TOKEN_BROADCAST_SUCCESS)
        filter.addAction(Constants.SCREEN_SHARING_TOKEN_BROADCAST_FAILURE)
        ssoBroadcastReceiver = SSOBroadcastReceiver()
        registerReceiver(ssoBroadcastReceiver, filter)

        buttonBindService = findViewById<View>(R.id.buttonBindService) as Button
        buttonUnBindService = findViewById<View>(R.id.buttonUnBindService) as Button
        buttonGetInfo = findViewById<View>(R.id.buttonInfo) as Button

        buttonGetInfo!!.setOnClickListener(this)
        buttonBindService!!.setOnClickListener(this)
        buttonUnBindService!!.setOnClickListener(this)

    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.buttonBindService -> bindToRomoteService()
            R.id.buttonUnBindService -> unbindFromRemoteSevice()
            R.id.buttonInfo -> fetchInfo()
        }
    }

    fun setResponseText(res: String) {
        Log.d(TAG, res)
    }

    private fun bindToRomoteService() {
        SSOServiceHelper.instance.bindWithService(this)
    }

    private fun unbindFromRemoteSevice() {
        SSOServiceHelper.instance.unbindService(this)
    }

    private fun fetchInfo() {
        // sending request through service
        //SSOServiceHelper.instance.fetchInfo()

        // sending broadcast
        val intent = Intent()
        intent.action = REQUEST_ACCESS_TOKEN_SCREENSHARING
        sendBroadcast(intent)
    }

    override fun onDestroy() {
        super.onDestroy()

        SSOServiceHelper.instance.unbindService(this)

        if (ssoBroadcastReceiver != null) {
            unregisterReceiver(ssoBroadcastReceiver)
            ssoBroadcastReceiver = null
        }
    }

}