package com.example.videoconferenceapp.serviceHelper

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log
import com.example.videoconferenceapp.constants.Constants
import com.example.videoconferenceapp.responseHandler.SSOResponseHandler

class SSOServiceHelper {
    companion object {
        private val TAG = "SSOServiceHelper"
        val instance = SSOServiceHelper()
    }

    private var myReceiveMessenger: Messenger? = null
    private val ssoServiceIntent = Intent()

    /**
     * Messenger for communicating with the SSOService.
     */
    private var ssoServiceMessenger: Messenger? = null

    /**
     * Flag indicating whether we have called bind on the SSOService.
     */
    var ssoServiceBound: Boolean = false

    fun bindWithService(context: Context) {

        ssoServiceIntent.component = ComponentName(
            "com.prowise.service.ssoauthenticator",
            "com.prowise.service.ssoauthenticator.SSOAuthenticationService"
        )
        context.bindService(ssoServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
        Log.d(TAG, "Service Bound")
    }

    fun unbindService(context: Context) {
        if (ssoServiceBound) {
            context.unbindService(serviceConnection)
            ssoServiceBound = false
            Log.d(TAG, "Service Un-Bound")
        }
    }

    /**
     * Class for interacting with the main interface of the SSOService.
     */
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            ssoServiceMessenger = Messenger(service)  // myRequestMessenger = Messenger(binder)
            myReceiveMessenger = Messenger(SSOResponseHandler())
            ssoServiceBound = true
            Log.d(TAG, "Service Connected")
        }

        override fun onServiceDisconnected(className: ComponentName) {
            ssoServiceMessenger = null  // myRequestMessenger = null
            myReceiveMessenger = null
            ssoServiceBound = false
            Log.d(TAG, "Service Not Able To Connect")
        }
    }

    fun fetchInfo() {
        if (ssoServiceBound) {
            val requestMessage1 = Message.obtain(null, Constants.MSG_IS_DEVICE_REGISTERED)
            val requestMessage2 = Message.obtain(null, Constants.MSG_IS_SERVICE_READY)
            val requestMessage3 = Message.obtain(null, Constants.MSG_IS_DEVICE_REGISTERED)
            requestMessage1.replyTo = myReceiveMessenger
            requestMessage2.replyTo = myReceiveMessenger

            try {
                ssoServiceMessenger!!.send(requestMessage1)
                ssoServiceMessenger!!.send(requestMessage2)
                ssoServiceMessenger!!.send(requestMessage3)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        } else {
            Log.d(TAG, "Service Unbound, can't get info\"")
        }
    }

    /*fun sendEnforcedAppIds(enforcedApps:String, enforceApps:Boolean)
    {
        if (appstoreServiceBound)
        {
            val bundle = Bundle()
            bundle.putString("enforcedAppIds",enforcedApps)
            bundle.putBoolean("enforceApps",enforceApps)
            val msg = Message()
            msg.what = MSG_IS_DEVICE_REGISTERED
            msg.data = bundle
            try
            {
                ssoServiceMessenger!!.send(msg)
            }
            catch (e: RemoteException)
            {
                e.printStackTrace()
            }
        }
    }*/
}