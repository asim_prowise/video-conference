package com.example.videoconferenceapp.model

data class ApiResData(
    val access_token: String,
    val org_id: String,
    val room: String,
    val user_id: String
)