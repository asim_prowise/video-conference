package com.example.videoconferenceapp.responseHandler

import android.os.Handler
import android.os.Message
import android.util.Log
import com.example.videoconferenceapp.CommuteActivity
import com.example.videoconferenceapp.constants.Constants

class SSOResponseHandler : Handler() {

    private val TAG = "MyServiceHelper"

    override fun handleMessage(msg: Message) {
        when (msg.what) {
            Constants.MSG_IS_DEVICE_REGISTERED -> {
                val responseData = msg.data["responseData"]
                Log.d(TAG, "MSG_IS_DEVICE_REGISTERED : $responseData")
                CommuteActivity.instance.setResponseText(responseData.toString())
            }
            Constants.MSG_IS_SERVICE_READY -> {
                val responseData = msg.data["responseData"]
                Log.d(TAG, "MSG_IS_SERVICE_READY : $responseData")
            }
        }
        super.handleMessage(msg)
    }
}