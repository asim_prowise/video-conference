package com.example.videoconferenceapp

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.videoconferenceapp.constants.Constants.MSG_SHOW_JITSI_ACTI_SRVCE

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jitsiActivityServiceIntent = Intent()
        jitsiActivityServiceIntent.component = ComponentName(
            "com.example.videoconferenceapp",
            "com.example.videoconferenceapp.VideoConferenceService"
        )
        bindService(jitsiActivityServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE)

        val jitsiBtn = findViewById<Button>(R.id.jitsiService)

        jitsiBtn.setOnClickListener {
            showJitsiActivityService()
            finish()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(serviceConnection)
    }

    /**
     * Messenger for communicating with the JitsiActivityService.
     */
    private var messenger: Messenger? = null

    /**
     * Flag indicating whether we have called bind on the JitsiActivityService.
     */
    private var bound: Boolean = false

    /**
     * Class for interacting with the main interface of the JitsiActivityService.
     */
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            messenger = Messenger(service)
            bound = true
        }

        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            messenger = null
            bound = false
        }
    }


    /*// This class handles the Service response
    private class ResponseHandler internal constructor(looper: Looper) : Handler(looper) {

        override fun handleMessage(msg: Message) {
            val respCode = msg.what

            when (respCode) {

            }
        }
    }*/

    private fun showJitsiActivityService() {
        if (bound) {
            // Create and send a message to the service, using a supported 'what' value
            val msg = Message()
            msg.what = MSG_SHOW_JITSI_ACTI_SRVCE
            //msg.replyTo = Messenger(ResponseHandler(Looper.getMainLooper()))
            try {
                messenger!!.send(msg)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }
    }


}
