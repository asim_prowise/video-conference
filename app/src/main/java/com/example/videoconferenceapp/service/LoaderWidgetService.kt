package com.example.videoconferenceapp.service

import android.app.Service
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import com.example.videoconferenceapp.R
import kotlinx.android.synthetic.main.layout_progress_widget.view.*

class LoaderWidgetService : Service() {

    private var layout_flag: Int = 0
    private lateinit var floatingView: View
    private lateinit var windowManager: WindowManager
    private val animationHandler = Handler()

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            layout_flag = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            layout_flag = WindowManager.LayoutParams.TYPE_PHONE
        }

        floatingView = LayoutInflater.from(this).inflate(R.layout.layout_progress_widget, null)

        var layoutParams = WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT,
            layout_flag,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )

        windowManager = getSystemService(WINDOW_SERVICE) as WindowManager
        windowManager.addView(floatingView, layoutParams)
        floatingView.visibility = View.VISIBLE

        startAnimation()

        return START_STICKY
    }

    private var shutdownAnimation: Runnable = object : Runnable {
        override fun run() {
            try {
                if (floatingView.shutdown_progress_animation != null) {
                    floatingView.shutdown_progress_animation.animateView()
                }
            } finally {
                animationHandler.postDelayed(this, 1000)
            }
        }
    }

    private fun startAnimation() {
        floatingView.shutdown_progress_animation.visibility = View.VISIBLE
        shutdownAnimation.run()
    }

    private fun stopAnimation() {
        animationHandler.removeCallbacks(shutdownAnimation)
        floatingView.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()

        stopAnimation()
        stopSelf()
    }

}