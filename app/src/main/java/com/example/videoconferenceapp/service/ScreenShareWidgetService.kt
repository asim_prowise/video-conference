package com.example.videoconferenceapp.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Debug
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.example.videoconferenceapp.R
import com.example.videoconferenceapp.RealActivity.Companion.instance
import kotlinx.android.synthetic.main.layout_share_widget.view.*


class ScreenShareWidgetService : Service() {

    private var layout_flag: Int = 0

    private lateinit var floatingView: View
    private lateinit var windowManager: WindowManager
    private var TAG = "ScreenShareWidgetService"
    private var audioCheck = false
    private var optionCheck = false
    private var openWindow = false
    private var barOpen = false
    private var pipEnabled = false
    private var isSharingMe = false
    private var screenShareButtonDisabled = false
    private val handler = Handler()
    private val delay = 1000 // 1000 milliseconds == 1 second

    companion object {
        var serviceInstance : ScreenShareWidgetService? = null
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            layout_flag = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            layout_flag = WindowManager.LayoutParams.TYPE_PHONE
        }

        floatingView = LayoutInflater.from(this).inflate(R.layout.layout_share_widget, null)

        serviceInstance = this

        val layoutParams = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            layout_flag,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )

        windowManager = getSystemService(WINDOW_SERVICE) as WindowManager
        windowManager.addView(floatingView, layoutParams)
        floatingView.visibility = View.VISIBLE

        floatingView.startStopPip.alpha = 0.5f
        floatingView.startStopPip.isEnabled = false

        Handler().post {
            val myFadeInAnimation : Animation = AnimationUtils.loadAnimation(this, R.anim.fadeanim)
            floatingView.memoryBar.startAnimation(myFadeInAnimation)
        }

        handler.postDelayed(object : Runnable {
            override fun run() {
                postMemoryDetails()
                handler.postDelayed(this, delay.toLong())
            }
        }, delay.toLong())

        floatingView.startStopPip.setOnClickListener {
            pipEnabled = !pipEnabled
            if (pipEnabled) {
                instance!!.startPictureInPicture()
                if (!isSharingMe) {
                    floatingView.videoOutputScreenShare.alpha = 0.5f
                    floatingView.videoOutputScreenShare.isEnabled = false
                    screenShareButtonDisabled = true
                    // TODO other solution : make it invisible -> full screen (screenShare enabled) -> getBack to Pip -> make it visible
                }
            } else {
                instance!!.stopPictureInPicture()
                if (screenShareButtonDisabled) {
                    floatingView.videoOutputScreenShare.alpha = 1f
                    floatingView.videoOutputScreenShare.isEnabled = true
                    screenShareButtonDisabled = false
                }
            }
            floatingView.optionsBtn.performClick()
        }

        floatingView.layoutStopShare.setOnClickListener {
            Log.d(TAG, "layoutStopShare clicked")
            stopSelf()
            instance!!.disconnectCall()
        }

        floatingView.audioBtn.setOnClickListener {
            Log.d(TAG, "audioBtn clicked")
            audioCheck = !audioCheck
            if (audioCheck) {
                instance!!.disableMicrophone(false)
            } else {
                instance!!.disableMicrophone(true)
            }
        }

        floatingView.optionsBtn.setOnClickListener {
            Log.d(TAG, "optionsBtn clicked")
            optionCheck = !optionCheck
            if (optionCheck) {
                floatingView.layoutVideoOptions.visibility = View.VISIBLE
            } else {
                floatingView.layoutVideoOptions.visibility = View.GONE
            }
        }

        floatingView.openCloseWindow.setOnClickListener {
            Log.d(TAG, "videoOutputCamera clicked")
            openWindow = !openWindow
            instance!!.openCloseWindow(openWindow)
            if (openWindow) {
                floatingView.openCloseWindow.setImageResource(R.drawable.ic_baseline_visibility_off)
                floatingView.startStopPip.alpha = 1f
                floatingView.startStopPip.isEnabled = true
            } else {
                floatingView.openCloseWindow.setImageResource(R.drawable.ic_baseline_visibility)
                floatingView.startStopPip.alpha = 0.5f
                floatingView.startStopPip.isEnabled = false
            }
            floatingView.optionsBtn.performClick()
        }

        floatingView.videoOutputScreenShare.setOnClickListener {
            Log.d(TAG, "videoOutputScreenShare clicked")
            instance!!.startStopScreenSharing()
            floatingView.optionsBtn.performClick()
        }

        floatingView.memoryBar.setOnClickListener {
            Log.d(TAG, "memoryBar clicked")
            barOpen = !barOpen
            if (barOpen) {
                floatingView.memoryConsumptionTxt.visibility = View.VISIBLE
            } else {
                floatingView.memoryConsumptionTxt.visibility = View.GONE
            }
        }

        floatingView.setOnTouchListener(object : OnTouchListener {

            var updatepar: WindowManager.LayoutParams = layoutParams
            var x = 0.0
            var y = 0.0
            var px = 0.0
            var py = 0.0
            override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
                when (motionEvent.action) {
                    MotionEvent.ACTION_DOWN -> {
                        x = updatepar.x.toDouble()
                        y = updatepar.y.toDouble()
                        px = motionEvent.rawX.toDouble()
                        py = motionEvent.rawY.toDouble()
                    }
                    MotionEvent.ACTION_MOVE -> {
                        updatepar.x = (x + (motionEvent.rawX - px)).toInt()
                        updatepar.y = (y + (motionEvent.rawY - py)).toInt()
                        windowManager.updateViewLayout(floatingView, updatepar)
                    }
                    else -> {
                    }
                }
                return false
            }
        })


        return START_STICKY
    }

    fun changeScreenShareUI(isScreenSharing: Boolean) {
        isSharingMe = isScreenSharing
        if (isScreenSharing) {
            floatingView.videoOutputScreenShare.setImageResource(R.drawable.ic_baseline_screen_share)
            floatingView.recordingAnimationView.visibility = View.VISIBLE

        } else {
            if (pipEnabled) {
                screenShareButtonDisabled = true
                floatingView.videoOutputScreenShare.alpha = 0.5f
                floatingView.videoOutputScreenShare.isEnabled = false
            } else screenShareButtonDisabled = false
            floatingView.videoOutputScreenShare.setImageResource(R.drawable.ic_baseline_stop_screen_share)
            floatingView.recordingAnimationView.visibility = View.GONE
        }
    }

    fun changeAudioUI(isAudioMuted: Boolean) {
        if (isAudioMuted) {
            floatingView.audioBtn.setImageResource(R.drawable.ic_baseline_mic_off)
        } else {
            floatingView.audioBtn.setImageResource(R.drawable.ic_baseline_mic)
        }

    }

    private fun postMemoryDetails() {
        val rt = Runtime.getRuntime()
        val vmAlloc = rt.totalMemory() - rt.freeMemory()
        val nativeAlloc: Long = Debug.getNativeHeapAllocatedSize()

        val memoryConsumption = formatMemoryText(nativeAlloc + vmAlloc).toFloat()
        val memoryConsumptionString = "$memoryConsumption mb"
        floatingView.memoryConsumptionTxt.text = memoryConsumptionString

        if (memoryConsumption < 100.0) {
            // color green
            floatingView.memoryBar.background = resources.getDrawable(R.drawable.rounded_line1)
        } else if (memoryConsumption in 101.0..149.0) {
            // color yellow
            floatingView.memoryBar.background = resources.getDrawable(R.drawable.rounded_line2)
        } else if (memoryConsumption in 150.0..199.0) {
            // color orange
            floatingView.memoryBar.background = resources.getDrawable(R.drawable.rounded_line3)
        } else if (memoryConsumption in 200.0..299.0) {
            // color orangish
            floatingView.memoryBar.background = resources.getDrawable(R.drawable.rounded_line4)
        } else {
            // color red
            floatingView.memoryBar.background = resources.getDrawable(R.drawable.rounded_line5)
        }

        /*Log.d(TAG, "rt.totalMemory() ${formatMemoeryText(rt.totalMemory())}")
        Log.d(TAG, "rt.freeMemory(): ${formatMemoeryText(rt.freeMemory())}")
        Log.d(TAG, "vmAlloc: ${formatMemoeryText(vmAlloc)}")
        Log.d(TAG, "nativeAlloc: ${formatMemoeryText(nativeAlloc)}")
        Log.d(TAG, "nativeAlloc + vmAlloc: ${formatMemoeryText(nativeAlloc + vmAlloc)}")
        Log.d(TAG, "memoryConsumption: $memoryConsumption")
        Log.d(TAG, "Debug.getNativeHeapSize(): ${formatMemoeryText(Debug.getNativeHeapSize())}")*/

    }

    private fun formatMemoryText(memory: Long): String {
        val memoryInMB = memory * 1f / 1024 / 1024
        return String.format("%.1f", memoryInMB)
    }

    override fun onDestroy() {

        serviceInstance = null

        super.onDestroy()

        floatingView.visibility = View.GONE
        stopSelf()
    }
}
