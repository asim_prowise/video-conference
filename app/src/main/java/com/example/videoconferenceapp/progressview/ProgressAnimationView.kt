package com.example.videoconferenceapp.progressview

import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.example.videoconferenceapp.R

class ProgressAnimationView : View {

    private var radius = 110f
    private var rotate = 45f
    private var backgroundPaint = Paint()
    private var animator = ValueAnimator()

    private var viewColor: Int = Color.BLACK
    private var viewSize: Float = 0.0f

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(set: AttributeSet?) {
        if (set == null) {
            return
        }

        val ta = context.obtainStyledAttributes(set, R.styleable.ProgressAnimationView)
        viewColor = ta.getColor(R.styleable.ProgressAnimationView_view_color, Color.BLACK)
        viewSize = ta.getDimension(R.styleable.ProgressAnimationView_view_size, resources.getDimension(R.dimen.update_view_progress_animation_size))
        ta.recycle()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        backgroundPaint.isAntiAlias = true
        backgroundPaint.color = Color.TRANSPARENT
        backgroundPaint.style = Paint.Style.FILL

        val viewWidth = viewSize / 2
        val viewHeight = viewSize / 2

        val left = viewWidth / 2
        val top = viewHeight / 2

        val right = viewWidth + left
        val bottom = viewHeight + top

        canvas.rotate(rotate, viewWidth, viewHeight)
        canvas.drawRoundRect(
            left,
            top,
            right,
            bottom,
            radius,
            radius,
            backgroundPaint
        )

        backgroundPaint.strokeWidth = 5f
        backgroundPaint.color = viewColor
        backgroundPaint.style = Paint.Style.STROKE
        // BORDER
        canvas.drawRoundRect(
            left,
            top,
            right,
            bottom,
            radius,
            radius,
            backgroundPaint
        )

        invalidate()
    }

    fun animateView() {
        val maxRadius = (viewSize / 4).toInt()
        val minRadius = (maxRadius * 0.73f).toInt()
        val propertyRadius = PropertyValuesHolder.ofInt(PROPERTY_RADIUS, maxRadius, minRadius, maxRadius)
        val propertyRotate = PropertyValuesHolder.ofInt(PROPERTY_ROTATE, 45, 135, 225)

        animator = ValueAnimator()
        animator.setValues(propertyRadius, propertyRotate)
        animator.duration = 1000
        animator.addUpdateListener { animation ->
            radius = (animation.getAnimatedValue(PROPERTY_RADIUS) as Int).toFloat()
            rotate = (animation.getAnimatedValue(PROPERTY_ROTATE) as Int).toFloat()
            invalidate()
        }
        animator.start()
    }

    companion object {
        private const val PROPERTY_RADIUS = "radius"
        private const val PROPERTY_ROTATE = "rotation"
    }
}
