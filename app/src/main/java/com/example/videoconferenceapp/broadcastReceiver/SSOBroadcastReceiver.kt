package com.example.videoconferenceapp.broadcastReceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.videoconferenceapp.CredentialsActivity.Companion.instance
import com.example.videoconferenceapp.constants.Constants.SCREEN_SHARING_TOKEN_BROADCAST_FAILURE
import com.example.videoconferenceapp.constants.Constants.SCREEN_SHARING_TOKEN_BROADCAST_SUCCESS

class SSOBroadcastReceiver : BroadcastReceiver() {

    private val TAG = "SSOBroadcastReceiver"

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        when (intent.action) {
            SCREEN_SHARING_TOKEN_BROADCAST_SUCCESS -> {
                val deviceToken = intent.getStringExtra("access_token").toString()
                instance.requestRoomAccess(deviceToken)
                Log.d(TAG, "token success = $deviceToken")
            }
            SCREEN_SHARING_TOKEN_BROADCAST_FAILURE -> {
                val failure = intent.getStringExtra("access_token").toString()
                instance.tokenFailure(failure)
                Log.e(TAG, "token failure = $failure")
            }
        }
    }
}