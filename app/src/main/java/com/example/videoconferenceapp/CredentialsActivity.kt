package com.example.videoconferenceapp

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.example.videoconferenceapp.broadcastReceiver.SSOBroadcastReceiver
import com.example.videoconferenceapp.constants.Constants
import com.example.videoconferenceapp.model.ApiResData
import com.example.videoconferenceapp.network.ApiClient
import com.example.videoconferenceapp.network.ApiService
import com.example.videoconferenceapp.service.LoaderWidgetService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CredentialsActivity : Activity() {

    private val TAG = "CredentialsActivity"
    private lateinit var intentLoaderWidget: Intent
    private var MEET_DEVICE_TOKEN = ""
    private var ACCESS_TOKEN = ""
    private var MEETING_ROOM_NAME = ""
    private var ssoBroadcastReceiver: SSOBroadcastReceiver? = null

    companion object {
        lateinit var instance: CredentialsActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_credentials)

        instance = this

        intentLoaderWidget = Intent(this, LoaderWidgetService::class.java)
        startService(intentLoaderWidget)

        when (BuildConfig.FLAVOR) {
            "einstein" -> Log.d(TAG, "Flavor : einstein")
            "eisler" -> Log.d(TAG, "Flavor : eisler")
            "hermes" -> Log.d(TAG, "Flavor : hermes")
        }

        val filter = IntentFilter()
        filter.addAction(Constants.SCREEN_SHARING_TOKEN_BROADCAST_SUCCESS)
        filter.addAction(Constants.SCREEN_SHARING_TOKEN_BROADCAST_FAILURE)
        ssoBroadcastReceiver = SSOBroadcastReceiver()
        registerReceiver(ssoBroadcastReceiver, filter)

        // sending broadcast
        val intent = Intent()
        intent.action = Constants.REQUEST_ACCESS_TOKEN_SCREENSHARING
        sendBroadcast(intent)

    }

    fun requestRoomAccess(tkn: String) {
        MEET_DEVICE_TOKEN = tkn
        try {
            ApiClient
                .getRoomClient()
                .create(ApiService::class.java)
                .getSSORoom("Bearer $MEET_DEVICE_TOKEN")
                .enqueue(object : Callback<ApiResData> {
                    override fun onResponse(call: Call<ApiResData>, response: Response<ApiResData>) {
                        if (response.isSuccessful) {
                            ACCESS_TOKEN = response.body()!!.access_token
                            MEETING_ROOM_NAME = response.body()!!.room

                            startRealActivity()
                        }
                    }

                    override fun onFailure(call: Call<ApiResData>, t: Throwable) {
                        Handler().postDelayed({
                            //Do something after 100ms
                            stopService(intentLoaderWidget)
                        }, 100)
                        Toast.makeText(this@CredentialsActivity, t.message, Toast.LENGTH_SHORT).show()
                        Log.e(TAG, "Error: ${t.message}")
                        finishAffinity()
                    }

                })

        } catch (e: Exception) {
            Handler().postDelayed({
                //Do something after 100ms
                stopService(intentLoaderWidget)
            }, 100)
            Toast.makeText(this@CredentialsActivity, e.toString(), Toast.LENGTH_SHORT).show()
            Log.e(TAG, "Error: $e")
            finishAffinity()
        }
    }

    fun tokenFailure(msg: String) {
        Handler().postDelayed({
            //Do something after 100ms
            stopService(intentLoaderWidget)
        }, 100)
        Toast.makeText(this@CredentialsActivity, msg, Toast.LENGTH_SHORT).show()
        Log.e(TAG, "Error: $msg")
        finishAffinity()
    }

    private fun startRealActivity() {
        val realIntent = Intent(this, RealActivity::class.java)
        realIntent.putExtra("ACCESS_TOKEN", ACCESS_TOKEN)
        realIntent.putExtra("MEETING_ROOM_NAME", MEETING_ROOM_NAME)
        startActivity(realIntent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (ssoBroadcastReceiver != null) {
            unregisterReceiver(ssoBroadcastReceiver)
            ssoBroadcastReceiver = null
        }
    }

}