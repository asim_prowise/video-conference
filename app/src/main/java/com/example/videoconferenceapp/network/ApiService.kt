package com.example.videoconferenceapp.network

import com.example.videoconferenceapp.model.ApiResData
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("jitsi")
    fun getSSORoom(
        @Header("Authorization") tkn : String
    ): Call<ApiResData>

}