package com.example.videoconferenceapp.constants

object Constants {

    const val BASE_LOGIN_URL = "https://login.test.prowise.io/"
    const val PROWISE_MEET_URL = "https://jitsi.test.prowise.io/"

    // MESSAGES
    //const val MSG_REQUEST_TOKEN_FOR_SCREEN_SHARING = 1
    const val MSG_IS_DEVICE_REGISTERED = 4
    const val MSG_IS_SERVICE_READY = 999
    const val MSG_START_JITSI_ACTIVITY = 1298
    const val MSG_CLOSE_SSO_VIEW = 1288
    const val MSG_SHOW_JITSI_ACTI_SRVCE = 1278
    const val MSG_START_CONF_DIALOG = 1268
    const val MSG_EXECUTE_COMMAND_WITH_ROOT = 1

    // BROADCASTS
    const val REQUEST_ACCESS_TOKEN_SCREENSHARING = "com.prowise.service.ssoauthenticator.request_access_token_screensharing"

    //BROADCAST ACTIONS
    const val SCREEN_SHARING_TOKEN_BROADCAST_SUCCESS = "com.prowise.service.screensharing.accesstoken.success"
    const val SCREEN_SHARING_TOKEN_BROADCAST_FAILURE = "com.prowise.service.screensharing.accesstoken.failure"

}
